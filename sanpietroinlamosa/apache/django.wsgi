import os
import sys
import site


paths = [ '/var/www/sanpietroinlamosabox/sanpietroinlamosa/',
          '/var/www/sanpietroinlamosabox/sanpietroinlamosa',
          '/var/www/sanpietroinlamosabox/lib/python2.7/site-packages/',
]

for path in paths:
    if path not in sys.path:
        sys.path.append(path)


# Add the site-packages of the chosen virtualenv to work with
site.addsitedir('/var/www/sanpietroinlamosabox/lib/python2.7/site-packages')

# Add the app's directory to the PYTHONPATH
sys.path.append('/var/www/sanpietroinlamosabox')
sys.path.append('/var/www/sanpietroinlamosabox/sanpietroinlamosa')

os.environ['DJANGO_SETTINGS_MODULE'] = 'sanpietroinlamosa.settings'

# Activate your virtual env
activate_env=os.path.expanduser("/var/www/sanpietroinlamosabox/bin/activate_this.py")
execfile(activate_env, dict(__file__=activate_env))

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
