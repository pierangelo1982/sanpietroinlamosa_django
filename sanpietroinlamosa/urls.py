"""sanpietroinlamosa URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
#from django.conf.urls import url
from django.conf.urls import include, url, patterns
from django.contrib import admin
from sito import views
from django.conf import settings
from django.conf.urls.static import static



urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.HomePage, name='home'),
    url(r'^pagina/(?P<post_id>\d+)/$', views.PageDetailView, name='pagina'),
    url(r'^categoria/(?P<post_id>\d+)/$', views.CategoriaView, name='categoria'),
    url(r'^attivita/(?P<post_id>\d+)/$', views.AttivitaView, name='attivita'),
    url(r'^collana/$', views.CollanaView, name='collana'),
    url(r'^luoghi/$', views.LuoghiView, name='luoghi'),
    url(r'^luoghi/(?P<post_id>\d+)/$', views.LuoghiDetailView, name='luogo'),
    url(r'^monastero/$', views.MonasteroView, name='monastero'),
    url(r'^monastero/(?P<post_id>\d+)/$', views.MonasteroDetailView, name='sale'),
    url(r'^contatti/$', views.ContattiView, name='contatto'),
    url(r'^partner/$', views.PartnerView, name='partner'),
    url(r'^galleria/$', views.GalleriaView, name='galleria'),
    url(r'^contact/$', views.contact, name='contact'),
    url(r'^success/$', views.success, name='success'),
    url(r'^cluniacensi/$', views.CluniacensiView, name='cluniacensi'),
    url(r'^news/$', views.NewsView, name='news'),
    url(r'^news/(?P<post_id>\d+)/$', views.NewsDetailView, name='news-dettaglio'),
    ##api
    url(r'^api/cluniacensi/$', views.cluniacensiJson, name='cluniacensiJson'), ##json
    ##tinymce
    url(r'^tinymce/', include('tinymce.urls')),


]

if settings.DEBUG:  
        urlpatterns += patterns('',  
                                #REMOVE IT in production phase  
                                (r'^media/(?P<path>.*)$', 'django.views.static.serve',  
                                {'document_root': settings.MEDIA_ROOT})
          )
