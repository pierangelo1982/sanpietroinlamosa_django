from django.contrib import admin
from sito.models import *


# Register your models here.
from image_cropping import ImageCroppingMixin

# Register your models here.

class MyModelAdmin(ImageCroppingMixin, admin.ModelAdmin):
    pass

class SliderAdmin(ImageCroppingMixin, admin.ModelAdmin):
    list_display = ("image_img", "titolo", "active")

class ImmaginiAdmin(ImageCroppingMixin, admin.ModelAdmin):
    list_display = ("image_img", "titolo")

def get_category(self):
    return self.categoria.all()
class AttivitaAdmin(ImageCroppingMixin, admin.ModelAdmin):
    list_display = ("image_img", "titolo", "cate", "inizio", "fine")

class PubblicazioniAdmin(ImageCroppingMixin, admin.ModelAdmin):
    list_display = ("image_img", "titolo", "paese", "numero_scheda","collana")

class NewsAdmin(ImageCroppingMixin, admin.ModelAdmin):
    list_display = ("titolo", "pub_date")


# Register your models here.
admin.site.register(Categorie)
admin.site.register(Immagini, ImmaginiAdmin)
admin.site.register(Video, MyModelAdmin)
admin.site.register(Attivita, AttivitaAdmin)
admin.site.register(Documenti, MyModelAdmin)
admin.site.register(Page, MyModelAdmin)
admin.site.register(Slider, SliderAdmin)
admin.site.register(Collana, MyModelAdmin)
admin.site.register(Pubblicazioni, PubblicazioniAdmin)
admin.site.register(Luoghi, ImmaginiAdmin)
admin.site.register(Attrezzature, MyModelAdmin)
admin.site.register(Monastero, ImmaginiAdmin)
admin.site.register(Partner, ImmaginiAdmin)
admin.site.register(Cluniacensi, ImmaginiAdmin)
admin.site.register(News, NewsAdmin)



