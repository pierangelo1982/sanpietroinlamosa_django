from __future__ import unicode_literals

from django.db import models
from image_cropping import ImageRatioField, ImageCropField

from datetime import datetime, timedelta, time, date
from django.utils.timesince import timesince

from filer.fields.image import FilerImageField
from filer.fields.file import FilerFileField
from filer.fields.folder import FilerFolderField

from tinymce.models import HTMLField

from django.utils import timezone

from django import forms



# Create your models here.

class Categorie(models.Model):
    titolo = models.CharField(max_length=100)
    sottotitolo = models.CharField(max_length=250, null=True, blank=True)

    def __unicode__(self):
        return self.titolo

    class Meta:
        verbose_name_plural = "Attivita Categorie"



class Immagini(models.Model):
    titolo = models.CharField(max_length=100, verbose_name="Titolo:")
    image = models.ImageField(blank=True, null=True, upload_to='uploaded_images')
    didascalia = models.TextField(null=True, blank=True)
    cropping = ImageRatioField('image', '500x480')
    slider = ImageRatioField('image', '1920x1280')
    revolution = ImageRatioField('image', '1170x500', verbose_name="Revolution Gallery")
    thumb = ImageRatioField('image', '595x335', verbose_name="Miniatura")
    croplibero = ImageRatioField('image', '595x335', free_crop=True, verbose_name="Ritaglio Libero")
    croppinguno = ImageRatioField('image', '1140x487', free_crop=True)
    croppingdue = ImageRatioField('image', '198x132')
    croppingtre = ImageRatioField('image', '1199x674')
    pub_date = models.DateTimeField('date published')

    def image_img(self):
        if self.image:
            return u'<img src="%s" style="width:200px"/>' % self.image.url
        else:
            return '(Sin imagen)'
    image_img.short_description = 'Thumb'
    image_img.allow_tags = True

    def __unicode__(self):
        return self.titolo

    class Meta:
        verbose_name_plural = "Galleria Immagini"
        ordering = ['id']



CSS_SLIDER = (
    ('0', 'stile uno'),
    ('1', 'stile due'),
                )
class Slider(models.Model):
    titolo = models.CharField(max_length=100, verbose_name="Titolo del Progetto:")
    image = models.ImageField(blank=True, null=True, upload_to='slider')
    scritta = models.TextField(null=True, blank=True)
    didascalia = models.TextField(null=True, blank=True)
    link = models.TextField(null=True, blank=True)
    cropping = ImageRatioField('image', '500x480')
    slider = ImageRatioField('image', '1920x1280')
    revolution = ImageRatioField('image', '1170x500', verbose_name="Revolution Gallery")
    thumb = ImageRatioField('image', '595x335', verbose_name="Miniatura")
    croplibero = ImageRatioField('image', '595x335', free_crop=True, verbose_name="Ritaglio Libero")
    croppinguno = ImageRatioField('image', '1140x487', free_crop=True)
    croppingdue = ImageRatioField('image', '198x132')
    croppingtre = ImageRatioField('image', '1199x674')
    pub_date = models.DateTimeField('date published')
    impaginazione = models.CharField(choices=CSS_SLIDER, max_length=200, null=True, blank=True)
    active = models.BooleanField('attiva',
                                    default=False)

    def image_img(self):
        if self.image:
            return u'<img src="%s" style="width:300px"/>' % self.image.url
        else:
            return '(Sin imagen)'
    image_img.short_description = 'Thumb'
    image_img.allow_tags = True

    def __unicode__(self):
        return self.titolo

    class Meta:
        verbose_name_plural = "Slider"
        ordering = ['id']



class Documenti(models.Model):
    titolo = models.CharField(max_length=100, verbose_name="Titolo")
    image = models.FileField(blank=True, null=True, upload_to='documenti')
    allegato = models.FileField(blank=True, null=True, upload_to='documents/%Y/%m/%d')
    didascalia = models.TextField(null=True, blank=True)
    pub_date = models.DateTimeField('date published')

    def __unicode__(self):
        return self.titolo

    class Meta:
        verbose_name_plural = "Documenti"
        ordering = ['id']



class Video(models.Model):
    titolo = models.CharField("Titolo del Video:", max_length=100, null=True, blank=True)
    codice = models.CharField("Codice ID YouTube:", max_length=100, null=True, blank=True)
    youtubeurl = models.CharField("Indirizzo url youtube:", max_length=100, null=True, blank=True)
    start = models.IntegerField(default=0, null=True, blank=True, verbose_name="Punto di Partenza del Filmato in secondi")
    embedded = models.TextField("Codice Embedded YouTube", null=True, blank=True)
    thumb = models.ImageField(upload_to='uploaded_thumb_youtube', null=True, blank=True)
    link = models.CharField(max_length=100, null=True, blank=True)
    didascalia = models.TextField(null=True, blank=True)

    def __unicode__(self):
        return self.titolo

    class Meta:
        verbose_name_plural = "Galleria Video"



class Attivita(models.Model):
    titolo = models.CharField("Titolo:", max_length=100, null=True, blank=True)
    titolomenu = models.CharField("Titolo Menu:", max_length=100, null=True, blank=True)
    revhome = models.BooleanField('Home Gallery',
                                    default=False,
                                    help_text="Mostra IMG nella slide in home")
    categoria = models.ManyToManyField(Categorie, null=True, blank=True)
    #inizio = models.DateField(null=True, blank=True, verbose_name="Inizio Evento")
    #fine = models.DateField( null=True, blank=True, verbose_name="Fine Evento")
    inizio = models.DateTimeField(null=True, blank=True, verbose_name="Inizio Evento")
    fine = models.DateTimeField( null=True, blank=True, verbose_name="Fine Evento")
    luogo = models.CharField(max_length=200,  null=True, blank=True,
    						verbose_name="Luogo dell'evento",
    						help_text="Esempio: San Pietro in Lamosa - sala 5")
    latitudine = models.CharField(max_length=200, null=True, blank=True, help_text="Non necessario")
    longitudine = models.CharField(max_length=200, null=True, blank=True, help_text="Non necessario")
    tel = models.CharField(max_length=200,  null=True, blank=True)
    web = models.CharField(max_length=200,  null=True, blank=True)
    email = models.CharField(max_length=200,  null=True, blank=True)
    introduzione = models.TextField(null=True, blank=True, verbose_name="Descrizione Breve per introduzione")
    #body = models.TextField(null=True, blank=True, verbose_name="Descrizione")
    body = HTMLField(null=True, blank=True, verbose_name="Descrizione")
    locandina = models.ImageField(blank=True, null=True, upload_to='attivita_locandine')
    image = models.ImageField(blank=True, null=True, upload_to='attivita')
    thumb = ImageRatioField('image', '600x600')
    cropping = ImageRatioField('image', '1200x1200')
    slider = ImageRatioField('image', '1920x1280')
    revolution = ImageRatioField('image', '1170x500')
    croplibero = ImageRatioField('image', '595x335', free_crop=True, verbose_name="Ritaglio Libero")
    album = FilerFolderField(null=True, blank=True)
    video = models.ManyToManyField(Video, null=True, blank=True)
    galleria = models.ManyToManyField(Immagini, null=True, blank=True, verbose_name="Seleziona Immagini Galleria")
    documenti = models.ManyToManyField(Documenti, null=True, blank=True, verbose_name="Documenti - Allegati")
    pub_date = models.DateTimeField('date published')

    def image_img(self):
        if self.image:
            return u'<img src="%s" style="width:200px"/>' % self.image.url
        else:
            return '(Sin imagen)'
    image_img.short_description = 'Thumb'
    image_img.allow_tags = True

    def cate(self):
    	a = "\n"
    	ca = self.categoria.all()
    	for c in ca:
    		a += c.titolo + "\n"
    	return a

    def delta(self):
        now = date.today()
        differenza = now - self.fine
        ok = differenza
        return ok

    def attesa(self):
        now = date.today()
        differenza = self.inizio - now
        ok = differenza
        return ok

    ## filtro x data
    #@property
    def is_past_due(self):
        if self.inizio > datetime.now():
            return True
        return False

    def __unicode__(self):
        return self.titolo

    class Meta:
        verbose_name_plural = "Attivita"
        ordering = ['-inizio']



class Page(models.Model):
    titolo = models.CharField("Titolo:", max_length=100, null=True, blank=True)
    titolomenu = models.CharField("Titolo Menu:", max_length=100, null=True, blank=True)
    revhome = models.BooleanField('Home Gallery',
                                    default=False,
                                    help_text="Mostra IMG nella slide in home")
    introduzione = models.TextField(null=True, blank=True, verbose_name="Descrizione Breve per introduzione")
    body = models.TextField(null=True, blank=True, verbose_name="Descrizione")
    image = models.ImageField(blank=True, null=True, upload_to='pagine')
    thumb = ImageRatioField('image', '600x600')
    cropping = ImageRatioField('image', '1200x1200')
    slider = ImageRatioField('image', '1920x1280')
    revolution = ImageRatioField('image', '1170x500')
    croplibero = ImageRatioField('image', '595x335', free_crop=True, verbose_name="Ritaglio Libero")
    album = FilerFolderField(null=True, blank=True)
    video = models.ManyToManyField(Video, null=True, blank=True)
    galleria = models.ManyToManyField(Immagini, null=True, blank=True, verbose_name="Seleziona Immagini Galleria")
    documenti = models.ManyToManyField(Documenti, null=True, blank=True, verbose_name="Documenti - Allegati")
    pub_date = models.DateTimeField('date published')

    def __unicode__(self):
        return self.titolo

    class Meta:
        verbose_name_plural = "Pagine"



class Collana(models.Model):
    titolo = models.CharField(max_length=100)
    sottotitolo = models.CharField(max_length=250, null=True, blank=True)
    casa_editrice = models.CharField(max_length=250, null=True, blank=True, verbose_name="casa editrice")

    def __unicode__(self):
        return self.titolo

    class Meta:
        verbose_name_plural = "Collana - Pubblicazioni"




class Pubblicazioni(models.Model):
    collana = models.ForeignKey(Collana, null=True, blank=True)
    titolo = models.CharField("Titolo:", max_length=100, null=True, blank=True)
    numero_scheda = models.CharField("Numero Scheda:", max_length=100, null=True, blank=True)
    paese = models.CharField("Paese:", max_length=100, null=True, blank=True,
        help_text="esempio: Fantecolo/Provaglio/Provezze")
    introduzione = models.TextField(null=True, blank=True, verbose_name="Descrizione Breve per introduzione")
    #body = models.TextField(null=True, blank=True, verbose_name="Descrizione")
    body = HTMLField(null=True, blank=True, verbose_name="Descrizione")
    image = models.ImageField(blank=True, null=True, upload_to='pubblicazioni')
    thumb = ImageRatioField('image', '460x800')
    cropping = ImageRatioField('image', '1200x1200')
    slider = ImageRatioField('image', '1920x1280')
    revolution = ImageRatioField('image', '1170x500')
    croplibero = ImageRatioField('image', '595x335', free_crop=True, verbose_name="Ritaglio Libero")
    album = FilerFolderField(null=True, blank=True)
    galleria = models.ManyToManyField(Immagini, null=True, blank=True, verbose_name="Seleziona Immagini Galleria")
    documenti = models.ManyToManyField(Documenti, null=True, blank=True, verbose_name="Documenti - Allegati")
    pub_date = models.DateTimeField('date published')

    def image_img(self):
        if self.image:
            return u'<img src="%s" style="width:200px"/>' % self.image.url
        else:
            return '(Sin imagen)'
    image_img.short_description = 'Thumb'
    image_img.allow_tags = True

    def __unicode__(self):
        return self.titolo

    class Meta:
        verbose_name_plural = "Pubblicazioni"



class Luoghi(models.Model):
    titolo = models.CharField("Titolo:", max_length=100, null=True, blank=True)
    titolomenu = models.CharField("Titolo Menu:", max_length=100, null=True, blank=True)
    '''
    latitudine = models.FloatField(null=True, blank=True)
    longitudine = models.FloatField(null=True, blank=True)
    via = titolomenu = models.CharField("Via:", max_length=100, null=True, blank=True)
    citta = models.CharField("citta:", max_length=100, null=True, blank=True)'''
    introduzione = models.TextField(null=True, blank=True, verbose_name="Descrizione Breve per introduzione")
    #body = models.TextField(null=True, blank=True, verbose_name="Descrizione")
    body = HTMLField(null=True, blank=True, verbose_name="Descrizione")
    image = models.ImageField(blank=True, null=True, upload_to='pagine')
    thumb = ImageRatioField('image', '600x600')
    cropping = ImageRatioField('image', '1200x1200')
    slider = ImageRatioField('image', '1920x1280')
    revolution = ImageRatioField('image', '1170x500')
    croplibero = ImageRatioField('image', '595x335', free_crop=True, verbose_name="Ritaglio Libero")
    album = FilerFolderField(null=True, blank=True)
    video = models.ManyToManyField(Video, null=True, blank=True)
    galleria = models.ManyToManyField(Immagini, null=True, blank=True, verbose_name="Seleziona Immagini Galleria")
    documenti = models.ManyToManyField(Documenti, null=True, blank=True, verbose_name="Documenti - Allegati")
    pub_date = models.DateTimeField('date published')

    def image_img(self):
        if self.image:
            return u'<img src="%s" style="width:200px"/>' % self.image.url
        else:
            return '(Sin imagen)'
    image_img.short_description = 'Thumb'
    image_img.allow_tags = True

    def __unicode__(self):
        return self.titolo

    class Meta:
        verbose_name_plural = "Luoghi"



class Attrezzature(models.Model):
    titolo = models.CharField(max_length=100)

    def __unicode__(self):
        return self.titolo

    class Meta:
        verbose_name_plural = "Attrezzature"



class Monastero(models.Model):
    titolo = models.CharField("Titolo:", max_length=100, null=True, blank=True)
    titolomenu = models.CharField("Titolo Menu:", max_length=100, null=True, blank=True)
    platea = models.CharField("Posti Platea:", max_length=100, null=True, blank=True)
    ferro_cavallo = models.CharField("Posti Ferro di Cavallo:", max_length=100, null=True, blank=True)
    posti = models.CharField("Posti Totali:", max_length=100, null=True, blank=True)
    attrezzature = models.ManyToManyField(Attrezzature, null=True, blank=True, verbose_name="Seleziona Attrezzature")
    introduzione = models.TextField(null=True, blank=True, verbose_name="Descrizione Breve per introduzione")
    #body = models.TextField(null=True, blank=True, verbose_name="Descrizione")
    body = HTMLField(null=True, blank=True, verbose_name="Descrizione")
    image = models.ImageField(blank=True, null=True, upload_to='pagine')
    thumb = ImageRatioField('image', '600x600')
    cropping = ImageRatioField('image', '1200x1200')
    slider = ImageRatioField('image', '1920x1280')
    revolution = ImageRatioField('image', '1170x500')
    croplibero = ImageRatioField('image', '595x335', free_crop=True, verbose_name="Ritaglio Libero")
    album = FilerFolderField(null=True, blank=True)
    video = models.ManyToManyField(Video, null=True, blank=True)
    galleria = models.ManyToManyField(Immagini, null=True, blank=True, verbose_name="Seleziona Immagini Galleria")
    documenti = models.ManyToManyField(Documenti, null=True, blank=True, verbose_name="Documenti - Allegati")
    pub_date = models.DateTimeField('date published')

    def image_img(self):
        if self.image:
            return u'<img src="%s" style="width:200px"/>' % self.image.url
        else:
            return '(Sin imagen)'
    image_img.short_description = 'Thumb'
    image_img.allow_tags = True

    def __unicode__(self):
        return self.titolo

    class Meta:
        verbose_name_plural = "Sale del Monastero"



class Partner(models.Model):
    titolo = models.CharField("Titolo:", max_length=100, null=True, blank=True)
    link = models.CharField("link:", max_length=100, null=True, blank=True)
    introduzione = models.TextField(null=True, blank=True, verbose_name="Descrizione Breve per introduzione")
    #body = models.TextField(null=True, blank=True, verbose_name="Descrizione")
    body = HTMLField(null=True, blank=True, verbose_name="Descrizione")
    image = models.ImageField(blank=True, null=True, upload_to='pagine')
    thumb = ImageRatioField('image', '600x600')
    cropping = ImageRatioField('image', '1200x1200')
    slider = ImageRatioField('image', '1920x1280')
    revolution = ImageRatioField('image', '1170x500')
    croplibero = ImageRatioField('image', '595x335', free_crop=True, verbose_name="Ritaglio Libero")
    pub_date = models.DateTimeField('date published')

    def image_img(self):
        if self.image:
            return u'<img src="%s" style="width:200px"/>' % self.image.url
        else:
            return '(Sin imagen)'
    image_img.short_description = 'Thumb'
    image_img.allow_tags = True

    def __unicode__(self):
        return self.titolo

    class Meta:
        verbose_name_plural = "Partner"


class Cluniacensi(models.Model):
    titolo = models.CharField("Titolo:", max_length=100, null=True, blank=True)
    titolomenu = models.CharField("Titolo Menu:", max_length=100, null=True, blank=True)
    latitudine = models.FloatField(null=True, blank=True)
    longitudine = models.FloatField(null=True, blank=True)
    via = models.CharField("Via:", max_length=100, null=True, blank=True)
    citta = models.CharField("citta:", max_length=100, null=True, blank=True)
    web = models.CharField("web:", max_length=100, null=True, blank=True)
    introduzione = models.TextField(null=True, blank=True, verbose_name="Descrizione Breve per introduzione")
    #body = models.TextField(null=True, blank=True, verbose_name="Descrizione")
    body = HTMLField(null=True, blank=True, verbose_name="Descrizione")
    image = models.ImageField(blank=True, null=True, upload_to='pagine')
    thumb = ImageRatioField('image', '600x600')
    cropping = ImageRatioField('image', '1200x1200')
    slider = ImageRatioField('image', '1920x1280')
    revolution = ImageRatioField('image', '1170x500')
    croplibero = ImageRatioField('image', '595x335', free_crop=True, verbose_name="Ritaglio Libero")
    album = FilerFolderField(null=True, blank=True)
    video = models.ManyToManyField(Video, null=True, blank=True)
    galleria = models.ManyToManyField(Immagini, null=True, blank=True, verbose_name="Seleziona Immagini Galleria")
    documenti = models.ManyToManyField(Documenti, null=True, blank=True, verbose_name="Documenti - Allegati")
    pub_date = models.DateTimeField('date published')

    def image_img(self):
        if self.image:
            return u'<img src="%s" style="width:200px"/>' % self.image.url
        else:
            return '(Sin imagen)'
    image_img.short_description = 'Thumb'
    image_img.allow_tags = True

    def __unicode__(self):
        return self.titolo

    class Meta:
        verbose_name_plural = "Siti Cluniacensi"


class News(models.Model):
    titolo = models.CharField("Titolo:", max_length=100, null=True, blank=True)
    titolomenu = models.CharField("Titolo Menu:", max_length=100, null=True, blank=True)
    introduzione = models.TextField(null=True, blank=True, verbose_name="Descrizione Breve per introduzione")
    body = HTMLField(null=True, blank=True, verbose_name="Descrizione")
    image = models.ImageField(blank=True, null=True, upload_to='news')
    thumb = ImageRatioField('image', '600x600')
    cropping = ImageRatioField('image', '1200x1200')
    slider = ImageRatioField('image', '1920x1280')
    revolution = ImageRatioField('image', '1170x500')
    croplibero = ImageRatioField('image', '595x335', free_crop=True, verbose_name="Ritaglio Libero")
    album = FilerFolderField(null=True, blank=True)
    video = models.ManyToManyField(Video, null=True, blank=True)
    galleria = models.ManyToManyField(Immagini, null=True, blank=True, verbose_name="Seleziona Immagini Galleria")
    documenti = models.ManyToManyField(Documenti, null=True, blank=True, verbose_name="Documenti - Allegati")
    pub_date = models.DateTimeField('date published')

    def __unicode__(self):
        return self.titolo

    class Meta:
        verbose_name_plural = "Blog-News"



## form
class ContactForm(forms.Form):
    nome = forms.CharField(label='Nome', max_length=100)
    cognome = forms.CharField(label='Cognome', max_length=100)
    email = forms.CharField(label='email', max_length=100)
