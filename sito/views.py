from django.shortcuts import render

from django.shortcuts import render_to_response, get_object_or_404
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.template import RequestContext
from django.views.generic import ListView, DetailView
from django.template.loader import render_to_string
from sito.models import *
from django.core.mail import send_mail
from filer.models import *

from datetime import datetime

from django.core import serializers





# Create your views here.
def HomePage(request):
    att_list = Attivita.objects.filter(revhome=True).order_by('-id')
    slider_list = Slider.objects.filter(active=True)
    context = {'slider_list':slider_list,
                'att_list':att_list}
    return render_to_response('index.html', context, context_instance=RequestContext(request))


def PageDetailView(request, post_id):
    page = Page.objects.get(pk=post_id)
    context = {'page': page}
    return render_to_response('pagina.html', context, context_instance=RequestContext(request))


def CategoriaView(request, post_id):
    categoria = Categorie.objects.get(pk=post_id)
    mytime = datetime.now()
    context = {'categoria': categoria,
                'mytime': mytime
                }
    return render_to_response('categorie.html', context, context_instance=RequestContext(request))


def AttivitaView(request, post_id):
    attivita = Attivita.objects.get(pk=post_id)
    filer_list = Image.objects.filter(folder_id = attivita.album)
    context = {'attivita': attivita,
                'filer_list': filer_list}
    return render_to_response('attivita.html', context, context_instance=RequestContext(request))


def CollanaView(request):
    collana_list = Collana.objects.all().order_by('id')
    context = {'collana_list': collana_list}
    return render_to_response('collana.html', context, context_instance=RequestContext(request))


def LuoghiView(request):
    luoghi = Luoghi.objects.all()
    context = {'luoghi': luoghi}
    return render_to_response('luoghi.html', context, context_instance=RequestContext(request))


def LuoghiDetailView(request, post_id):
    luogo = Luoghi.objects.get(pk=post_id)
    filer_list = Image.objects.filter(folder_id = luogo.album)
    context = {'luogo': luogo,
                'filer_list': filer_list}
    return render_to_response('luogo.html', context, context_instance=RequestContext(request))


def MonasteroView(request):
    monastero_list = Monastero.objects.all()
    context = {'monastero_list': monastero_list}
    return render_to_response('monastero.html', context, context_instance=RequestContext(request))


def MonasteroDetailView(request, post_id):
    monastero = Monastero.objects.get(pk=post_id)
    filer_list = Image.objects.filter(folder_id = monastero.album)
    context = {'monastero': monastero,
                'filer_list': filer_list}
    return render_to_response('stanze.html', context, context_instance=RequestContext(request))


def ContattiView(request):
    return render_to_response('contatti.html', context_instance=RequestContext(request))


def PartnerView(request):
    partner_list = Partner.objects.all()
    context = {'partner_list': partner_list}
    return render_to_response('partner.html', context, context_instance=RequestContext(request))


def GalleriaView(request):
    filer_list = Image.objects.all
    context = {'filer_list': filer_list}
    return render_to_response('galleria.html', context, context_instance=RequestContext(request))


def CluniacensiView(request):
    cluniacensi_list = Cluniacensi.objects.all()
    context = {'cluniacensi_list': cluniacensi_list}
    return render_to_response('cluniacensi.html', context, context_instance=RequestContext(request))

def NewsView(request):
    news_list = News.objects.all().order_by('-pub_date')
    context = {'news_list': news_list}
    return render_to_response('news.html', context, context_instance=RequestContext(request))


def NewsDetailView(request, post_id):
    news = News.objects.get(pk=post_id)
    filer_list = Image.objects.filter(folder_id = news.album)
    context = {'news': news,
                'filer_list': filer_list}
    return render_to_response('news_dettaglio.html', context, context_instance=RequestContext(request))



def contact(request):
    if request.method == 'POST': # If the form has been submitted...
        form = ContactForm(request.POST) # A form bound to the POST data
        if form.is_valid(): # All validation rules pass
            subject = 'Iscrizione Newsletter sanpietroinlamosa'
            #message = form.cleaned_data['messaggio']
            message = render_to_string('contact.txt', {'post': request.POST})
            sender = form.cleaned_data['email']
            cc_myself = False

            recipients = ['pierangelo1982@gmail.com']
            if cc_myself:
                recipients.append(sender)
        
            send_mail(subject, message, sender, recipients)
            return HttpResponseRedirect('/success/') # Redirect after POST
    else:
        form = ContactForm() # An unbound form

    #return render_to_response('contact.html', {'form': form,})
    return render_to_response('contact.html', context_instance=RequestContext(request))


def success(request):
    return render_to_response('success.html', context_instance=RequestContext(request))


#### JSON
def cluniacensiJson(request):
    data = serializers.serialize("json", Cluniacensi.objects.all())
    return HttpResponse(data, content_type='application/json;')  

    


